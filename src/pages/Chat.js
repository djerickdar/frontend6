import React, {useEffect} from 'react'

import Dashboard from '../components/Dashboard'

import { useHistory } from "react-router-dom";

import { useConversations } from '../contexts/ConversationsProvider'

export default function Chat() {

	const {id} = useConversations()

	let history = useHistory();
	
	useEffect(()=> {

		if( String(id) === 'undefined' || String(id) ==='null' ) {

			history.push('/')
		}	
	})
	
	return (
		<React.Fragment>
			{id ?  <Dashboard id={id}/> : null}
		</React.Fragment>
	)
}
