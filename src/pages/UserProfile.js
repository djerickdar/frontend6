import React, { useState, useEffect } from 'react';
import {Row, Col, Image, Alert  } from 'react-bootstrap';

import { Link, useRouteMatch} from "react-router-dom";

import MyItem from '../components/MyItem';
import towns from '../data/towns';
import regions from '../data/regions';
import countries from '../data/countries';


export default function UserProfile() {	

	const [allItems, setAllItems] = useState([])


	const [seller, setSeller] = useState({
		photo : null,
		firstName : null,
		lastName : null,
		email : null,
		town : null,
		region : null,
		country : null
	})


	const match = useRouteMatch({
		path: "/user/:userId",
		strict: true,
		sensitive: true
	});
	
	useEffect(()=>{

        let dCountry = null;
		let dTown = null;
		let dRegion = null;

        fetch(`${process.env.REACT_APP_API_URL}/users/user_details/${match.params.userId}`) 
		.then(res => res.json())
        .then(data2 => {
        	console.log('user data:',data2)
        	if(data2._id!==null || data2._id!==undefined) {

        		dCountry = countries.find((val)=>{return val.country===data2.country})
        		dTown = towns.find((val)=>{return val.town===data2.town})
        		dRegion = regions.find((val)=>{return val.region===data2.region})

				setSeller({
					photo : data2.photo,
					firstName : data2.firstName,
					lastName : data2.lastName,
					email : data2.email,
					town : dTown.townN,
					region : dRegion.regionN,
					country : dCountry.countryN
				})

				fetch(`${process.env.REACT_APP_API_URL}/item/listing/${match.params.userId}`) 
				.then(res => res.json())
		        .then(data1 => {
		        	
		        	if(data1.length>0){
		        		
		        		setAllItems(data1)
		        	}
		        })

        	}
        })

	},[])
	

	const items_Array = allItems.map((element, idx) => {

		if (element.isActive) {
			return {
				isActive: element.isActive,
				_id: element._id,
				category: element.category,
				condition: element.condition,
				createdOn: element.createdOn,
				description: element.description,
				photos: element.photos,
				price: element.price,
				subCategory: element.subCategory,
				title: element.title,
				userId: element.userId,
				num: parseInt(idx)
			}
		}
	})

	const sorting = items_Array.sort((a,b) => {
		if (a.num < b.num) {
			
			return 1
			
		} else if (a.num > b.num) {
			
			return -1
			
		} else {
			return 0
		}
	})

	const theItems = sorting.map(element => {
		// console.log(element)
		if(element.isActive && String(element.isActive)!== 'undefined') {
			
			return (
				<MyItem key={element._id} dataPropItem={element}/>
			)
		}
	})


	return (
		<React.Fragment>
			<Row className="my-5 pb-4 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0">
					
					<Row noGutters>
						<div className="bb py-4">
							<div className="bcd">
								<Image 
									className="itemE2_img1 pPic"
									src={`${seller.photo}`}/>
							</div>
							
							<div className="bc mt-3">
								{String(seller.firstName)!=='' &&String(seller.firstName)!=='null' && String(seller.firstName)!== 'undefined' ?
								<React.Fragment>
									<Link className="_navlinkReset">
										<p className="fnt2">
											{`${seller.firstName} ${seller.lastName}`}
										</p>
									</Link>
									<br/>
									<div className="mb-2">
										{`${'Email:'} ${seller.email}`}
									</div>
									<p>
										{`${seller.town}, ${seller.region}`}
										<br/>
										{`${seller.country}`}
									</p>
								</React.Fragment>
								:
								null
								}
							</div>
						</div>

						<div className="c shadow">
							<div className="urlDiv2">
								<p className="fnt1 pl-3 pt-3">Listing</p>
								<div className="homeDiv1 homeDiv1_v2">
									{theItems.length>0 ?theItems: <Alert variant="warning">No ITems yet.</Alert> }
								</div>
						</div>
						</div>
						
					</Row>
				</Col>
			</Row>
		</React.Fragment>
	)
};
