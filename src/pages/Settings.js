import React, { useState, useEffect } from 'react';
import {Row, Col, Image, Button, Form, Spinner } from 'react-bootstrap';

import { useGlobalData } from '../UserProvider';

import towns from '../data/towns';
import regions from '../data/regions';

import { RiArrowDownSLine, RiArrowRightSLine } from "react-icons/ri";
import { MdAddToPhotos } from "react-icons/md";

import { useHistory } from "react-router-dom";

import Swal from 'sweetalert2';

export default function Settings () {
	const { refreshData, setRefreshData } = useGlobalData();

	const [firstName, setFirstName] = useState(localStorage.getItem('firstName'))
	const [lastName, setLastName] = useState(localStorage.getItem('lastName'))
	const [email, setEmail] = useState(localStorage.getItem('email'))
	const [mobileNo, setMobileNo] = useState(localStorage.getItem('mobileNo'))
	const [country, setCountry] = useState(localStorage.getItem('country'))
	const [region, setRegion] = useState(localStorage.getItem('region'))
	const [town, setTown] = useState(localStorage.getItem('town'))
	const [photo, setPhoto] = useState(localStorage.getItem('photo'))
	const [photo1, setPhoto1] = useState('')


	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [password3, setPassword3] = useState('')

	const [passVerify, setPassVerify] = useState('settings-b3')
	const [passFeedback, setPassFeedback] = useState('d-none')
	const [disableButton, setDisableButton] = useState(false)
	
	const [shiftPage, setShiftPage] = useState(true)
	
	let history = useHistory();

	const myRef = React.createRef();

	const _setPage1 = () => setShiftPage(true);
    const _setPage2 = () => setShiftPage(false);


	// to update user profile	 
	function userProfile (e) {
		e.preventDefault()
		// console.log('update profile town: ',town)
		const userId = localStorage.getItem('userId')
		setDisableButton(true)
		fetch(`${process.env.REACT_APP_API_URL}/users/update-profile/${userId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName :lastName,
				email: email,
				mobileNo: mobileNo,
				country: country,
				region: region,
				town: town,
				photo: photo1!==''?{ new:true, data:photo1 }: { new:false, data:photo }
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.noerror) {
				console.log("Good!,", data)
				fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		            headers: {
		                Authorization: `Bearer ${localStorage.getItem('token')}`
		            }
		        })	
		        .then(res => res.json())
		        .then(data => {
		        	console.log('update profile: ',data)
		            if(data._id !== null && data._id !== 'error' && data._id !== undefined) {
		            	
		                localStorage.setItem('firstName', data.firstName)
		                localStorage.setItem('lastName', data.lastName)
		                localStorage.setItem('email', data.email)
		                localStorage.setItem('mobileNo', data.mobileNo)
		                localStorage.setItem('country', data.country)
		                localStorage.setItem('region', data.region)
		                localStorage.setItem('town', data.town)
		                localStorage.setItem('photo', data.photo)
		                
		                setFirstName( localStorage.getItem('firstName'))
						setLastName( localStorage.getItem('lastName'))
						setEmail( localStorage.getItem('email'))
						setMobileNo( localStorage.getItem('mobileNo'))
						setCountry( localStorage.getItem('country'))
						setRegion( localStorage.getItem('region'))
						// setTown( localStorage.getItem('town'))
						setPhoto( localStorage.getItem('photo'))
						setPhoto1('')

						setRefreshData(!refreshData)
						setDisableButton(false)

						
		        	} else {

		        		localStorage.clear();
		        		history.push('/')
		        		setRefreshData(!refreshData)
		        		setDisableButton(false)
		        	}
		            
		        })
			} else {
				console.log("Opps Error!,", data)
				Swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'Internal server error.',
                  showConfirmButton: false,
                  timer: 5000
                })
                setRefreshData(!refreshData)
                setDisableButton(false)
			}
		})
	}

	function userPassword (e) {
		e.preventDefault()
		
		setDisableButton(true)
		fetch(`${process.env.REACT_APP_API_URL}/users/check-password`, {
			method: 'POST',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			
		
			if(data) {

				fetch(`${process.env.REACT_APP_API_URL}/users/change-password`, {
					method: 'PUT',
					headers: {
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						password: password2
					})
				})
				.then(res => res.json())
				.then(data => {
					
					setPassword1('')
					setPassword2('')
					setPassword3('')

					setDisableButton(false)
					
					Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Password Saved',
                      showConfirmButton: false,
                      timer: 1500
                    })
				})

			} else {

				setPassword1('')
				setPassword2('')
				setPassword3('')

				setDisableButton(false)
				
				Swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'incorrect current password',
                  showConfirmButton: false,
                  timer: 1500
                })
			}

		})
	}

	const uploadPhoto = () => {
		
		const reader = new FileReader();
		const file = myRef.current.files[0]

		if(file) { reader.readAsDataURL(file) }

		reader.onload = () => {
			const readerUrl = reader.result;
			
			
			setPhoto1(readerUrl)
		}
		reader.onerror = (error) => {
			console.log('got an error while uploading', error)
		}
	}

	const selectRegion = regions.map((element, idx)=>{

		if(element.country===country){
			return( <option key={idx} value={element.region}>{element.regionN}</option> )
		}
		return ( <option key={'000'} value={''}>Select region</option> )
	})

	const selectTown = towns.map((element, idx)=>{

		if(element.region===region&&element.country===country){
			return( <option key={idx} value={element.town}>{element.townN}</option> )
		}

		return( <option key={idx} value={'000'}>Select town</option> )
	})

	useEffect (()=> {
		
		if(shiftPage) {
			setPassword1('')
			setPassword2('')
			setPassword3('')
		} else {
			setFirstName(localStorage.getItem('firstName'))
			setLastName(localStorage.getItem('lastName'))
			setEmail(localStorage.getItem('email'))
			setMobileNo(localStorage.getItem('mobileNo'))
			setCountry(localStorage.getItem('country'))
			setRegion(localStorage.getItem('region'))
			setTown(localStorage.getItem('town'))
			setPhoto(localStorage.getItem('photo'))
			setPhoto1('')
		}
	}, [shiftPage])

	useEffect(() => {
        if((password2 !== '' && password3 !== '') && (password2 === password3)){
            setPassVerify('settings-b3 isValid')
			setPassFeedback('settings-b2-valid')
			
        }else if((password2 !== '' && password3 !== '') && (password2 !== password3)) {
            setPassVerify('settings-b3 isInvalid')
			setPassFeedback("settings-b2-invalid")
			
        } else {
        	setPassVerify('settings-b3')
			setPassFeedback('d-none')
			
        }
        
    }, [password2, password3])

   
	useEffect(()=>{
		console.log('town:_', town)
	},[town])

	return (
		<React.Fragment>
			
			<Row className="my-5 pb-4 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0">
					
					<Row noGutters>
						<div className="bb bb-prof">
							<button className="bc settings-btn" onClick={_setPage1}>
								Profile Settings
								<RiArrowRightSLine className="settings-icon icon-a"/>
								<RiArrowDownSLine className="settings-icon icon-b"/>
							</button>
							{ localStorage.getItem('loginType') === "google" ?
								null
								:	
								<button className="bc settings-btn" onClick={_setPage2}>
									Change password
									<RiArrowRightSLine className="settings-icon icon-a"/>
									<RiArrowDownSLine className="settings-icon icon-b"/>
								</button>
							}
						</div>

						<div className="c shadow mb-5 pb-4">
							<div className="urlDiv2">
								<div className="homeDiv1 homeDiv1_v2 pt-4">

							{ shiftPage ?
								<React.Fragment>
									<p className="fnt1">Profile</p>
									<Form onSubmit={(e) => userProfile(e)} className="setting-prof">

										<div className="settings-pp my-3">
											
											{
												photo1!=='' ?
												<Image className="settings-img" src={photo1}/>
												:
												<Image className="settings-img" src={`${photo}`}/>
											}

											{<div className="settings-pp-btn">
												<input 
													type="file" 
													id="sell-3-input"
													value={""}
													ref={myRef}
													accept="image/*"
													onChange= {uploadPhoto}
													className="displayNone"/>

												<label 
													htmlFor="sell-3-input" 
													id="sell-3-label"
													className="btn btn-primary"
												>
													<MdAddToPhotos/>
												</label>
											</div>}

										</div>

										
										<div className="settings-b1 my-5">
											<label className="settings-b2" htmlFor="s-txtbox1">
											  	First name
											</label>
											<input
										  		id="s-txtbox1"
									        	type="text" 
									        	placeholder="First name" 
									        	value={firstName}
									        	onChange={e => setFirstName(e.target.value)} 
									        	className="settings-b3"/>
										</div>

										<div className="settings-b1 mb-5">
											<label className="settings-b2" htmlFor="s-txtbox2">
											  	Last name
											</label>
											<input
										  		id="s-txtbox2"
									        	type="text" 
									        	placeholder="Last name" 
									        	value={lastName}
									        	onChange={e => setLastName(e.target.value)} 
									        	className="settings-b3"/>
										</div>

										<div className="settings-b1 mb-5">
											<label className="settings-b2" htmlFor="s-txtbox4">
											  	Phone number
											</label>
											<input
										  		id="s-txtbox4"
									        	type="number" 
									        	placeholder="Phone number" 
									        	value={mobileNo}
									        	onChange={e => setMobileNo(e.target.value)} 
									        	className="settings-b3"/>
										</div>
										
										<div className="settings-b1 mb-5">
											<label className="settings-b2" htmlFor="s-txtbox5">
											  	Country
											</label>
											<select 
												id="s-txtbox5" className="settings-b3" 
												onChange={e => setCountry(e.target.value)}
												defaultValue={country}>		
												<option value="">Select a country</option>
										        <option value="philippines">Philippines</option>
									        </select>
										</div>

										<div className="settings-b1 mb-5">
											<label className="settings-b2" htmlFor="s-txtbox6">
											  	Region
											</label>
											<select 
												id="s-txtbox6" className="settings-b3" 
												onChange={e => setRegion(e.target.value)}
												defaultValue={region}>		
												<option value="">Select a region</option>
										        {selectRegion}
									        </select>
										</div>

										<div className="settings-b1 mb-5">
											<label className="settings-b2" htmlFor="s-txtbox7">
											  	Town
											</label>
											<select 
												id="s-txtbox7" className="settings-b3" 
												onChange={e => setTown(e.target.value)}
												defaultValue={town}>		
												<option value="">Select a town</option>
										        {selectTown}
									        </select>
										</div>
										<Row noGutters className="justify-content-end">
											{disableButton ? 
												<Button 
													id="btnUserProfile" 
													variant="success" 
													size="md" 
													type="submit" disabled>Save
													<Spinner 
								                		animation="border" 
								                		variant="light"
								                		className="ml-4" 
								                		size="sm" 
								                		role="status"/>
												</Button>
												:
												<Button 
													id="btnUserProfile" 
													variant="success" 
													size="md" 
													type="submit">Save
												</Button>
											}
										</Row>
									</Form>
								</React.Fragment>
							: 	

								localStorage.getItem('loginType') !== "google" ?
								<React.Fragment>
									<p className="fnt1">Change password</p>
									<Form onSubmit={(e) => userPassword(e)} className="setting-prof">
									    <div className="settings-b1 my-5">
											<label className="settings-b2" htmlFor="s-txtbox11">
											  	Current password
											</label>
											<input
										  		id="s-txtbox11"
									        	type="password" 
									        	placeholder="" 
									        	value={password1}
									        	onChange={e => setPassword1(e.target.value)} 
									        	className="settings-b3"/>
										</div>
										<div className="settings-b1 mb-5">
											<label className="settings-b2" htmlFor="s-txtbox12">
											  	New password
											</label>
											<input
										  		id="s-txtbox12"
									        	type="password" 
									        	placeholder="" 
									        	value={password2}
									        	onChange={e => setPassword2(e.target.value)} 
									        	className="settings-b3"/>
										</div>
										<div className="settings-b1 mb-5">
											<label className="settings-b2" htmlFor="s-txtbox13">
											  	Confirm password
											</label>
											<input
										  		id="s-txtbox13"
									        	type="password" 
									        	placeholder="" 
									        	value={password3}
									        	onChange={e => setPassword3(e.target.value)} 
									        	className={`${passVerify}`}/>
											{
								        		passFeedback === 'settings-b2-invalid'?
									        		<span className= {`${passFeedback}`} >
										        		Password not matched
													</span>
												:
													<span className= {`${passFeedback}`} >
										        		Password matched
													</span>
												
											}
										</div>

										<Row noGutters className="justify-content-end">
											{disableButton ? 
												<Button 
													id="btnUserProfile" 
													variant="success" 
													size="md" 
													type="submit" disabled>Save
													<Spinner 
								                		animation="border" 
								                		variant="light"
								                		className="ml-4" 
								                		size="sm" 
								                		role="status"/>
												</Button>
												:
												<Button 
													id="btnUserProfile" 
													variant="success" 
													size="md" 
													type="submit">Save
												</Button>
											}
										</Row>
									</Form>
								</React.Fragment>
								:
								null
								
							}

								</div>
							</div>
						</div>
						
					</Row>
				</Col>
			</Row>

	
		
		</React.Fragment>
	)
};


	
