import React, { useState, useEffect } from 'react';


import Categories from '../components/Categories';

import UpdateItem from '../components/UpdateItem';

import towns from '../data/towns';
import regions from '../data/regions';
import countries from '../data/countries';

import { Row, Col, Image, Button, Modal } from 'react-bootstrap';

import { useGlobalData } from '../UserProvider';
import { useConversations } from '../contexts/ConversationsProvider'

import { Link, useHistory, useRouteMatch } from "react-router-dom";

// item details
export default function ItemDetails () {

	const { setShow1, addressName } = useGlobalData()
	
	const { createConversation, id, numComma, my } = useConversations()
	


	let history = useHistory();

	const match = useRouteMatch({
		path: "/item/:userId/:itemId",
		strict: true,
		sensitive: true
	});


	const [itemOwner, setItemOwner] = useState({
			photo:null,
			firstName: null,
			lastName: null,
			town: null,
			region: null,
			country: null
		})

	const [photos, setPhotos] = useState([])
	
	const [item, setItem] = useState({
			category:null,
			condition:null,
			createdOn:null,
			description:null,
			isActive:null,
			photos:null,
			price:null,
			subCategory:null,
			title:null,
			userId:null,
			_id:null,
	})

	const [firstPic, setFirstPic] = useState('')

	const [modalOpen, setModalOpen] = useState(false)

	const closeModal = ()=> {setModalOpen(false)}



	useEffect(()=>{
		
		localStorage.removeItem('item_userId')
	    localStorage.removeItem('item_price')
	    localStorage.removeItem('item_userFirstName')
	    localStorage.removeItem('convoRefId')

		let addr ={
			town:'',
			region:'',
			country:''
		}
		
		let myAddr = null;
	    
		fetch(`${process.env.REACT_APP_API_URL}/item/${match.params.itemId}`) 
		.then(res => res.json())
        .then(data => {
        	
        	if(String(data)!=='null' && String(data)!=='undefined' && data!==false) {

       	
	        	localStorage.setItem('item_userId', data.user._id)
	        	localStorage.setItem('item_userFirstName', data.user.firstName)

	        	localStorage.setItem('convoRefId',data.item._id+id+data.user._id)
	        	localStorage.setItem('_itemId',data.item._id)

	        	
				setPhotos(data.item.photos)
			
				setItem({
					category:data.item.category,
					condition:data.item.condition,
					createdOn:data.item.createdOn,
					description:data.item.description,
					isActive:data.item.isActive,
					photos:data.item.photos,
					price:numComma(data.item.price),
					subCategory:data.item.subCategory,
					title:data.item.title,
					userId:data.item.userId,
					_id:data.item._id
				})

				addr ={
					town:data.user.town,
					region:data.user.region,
					country:data.user.country
				}

				myAddr = addressName(addr);
				
				setItemOwner({
					photo:data.user.photo,
					firstName: data.user.firstName,
					lastName: data.user.lastName,
					town: myAddr.town,
					region: myAddr.region,
					country: myAddr.country
				})
		
			}else{
				history.push(`/error`)
			}
        })
        
	},[])



	useEffect(()=>{
		
		if(photos.length>0) {

			let tempurl = '';

			photos.forEach((element,idx) => {
				if(idx===0){tempurl=element.url}else{return null}
			})

			setFirstPic(tempurl)
		}
	}, [photos])

	const photosArr = photos.map((element, idx) => {
		
		return (
			<Image 
				key = {element._id}
				className="itemD3_img2" 
				src={element.url}/>
		)
	})

	
	const chat = ()=> {
		
		
		const item_userId = localStorage.getItem('item_userId');
		const item_userFirstName = localStorage.getItem('item_userFirstName');
		const convoRefId = localStorage.getItem("convoRefId")
		const itemId = localStorage.getItem('_itemId')

		if(localStorage.getItem('email') !== null && localStorage.getItem('email') !== 'undefined') {

			
			createConversation(
				{  
					item: item,
					itemId: itemId,
					convoRefId:convoRefId, 
					id:item_userId, 
					name: item_userFirstName
				})
			
    		history.push('/chat')
    	} else {

    		// show modal for login
    		setShow1(true)
    		
    	}
	}
	function deleteItem(){
		
		const itemId = localStorage.getItem('_itemId')
		
		fetch(`${process.env.REACT_APP_API_URL}/item/delete-item/${itemId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
        })
        .then(res => res.json())
        .then(data => {
        	
			if(data){
				// history.push(`/user/${my.id}`)
				window.location.assign(`/user/${my.id}`);
			}
		})
		
	}

	function editItem(){ setModalOpen(true) }

	return (
		<React.Fragment>
			
			<Row className="my-5 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0 ">
					<Row className="mb-4 mt-1 px-3" noGutters>
						<Col xs={12}>
							<span  className="lgTxt">CATEGORIES</span>
						</Col>
					</Row>
					<Row noGutters>
						<Col xs={12}>
							<Categories/>
						</Col>
					</Row>
				</Col>
			</Row>

			<Row className="my-5 pb-4 bgTransparent shadow" noGutters>
				<Col xs={12} className="p-0">
					<Row noGutters>
						<div className="itemC1">

							<div className="itemD1 borderGrey bgWhite">
								<div className="itemD2 ">
									<Image 
										className="itemD3_img1" 
										src={`${firstPic}`}/>
									{/*<Image 
										className="itemD3_img1" 
										src="/items/item_Img.png"/>*/}
								</div>
								<Row noGutters className="d-flex mt-2">
									<div className="itemD4 ">
										{photosArr}
									</div>
								</Row>
							</div>

							<div className="itemE1 bgWhite">
								
								<Row noGutters>
									<Col xs={6}>
										<Link 
											to={`/user/${localStorage.getItem('item_userId')}`}
											className="_navlinkReset">
											<Image 
												className="itemE2_img1" 
												src={`${itemOwner.photo}`}/>
											
											<span>
												{`${itemOwner.firstName} ${itemOwner.lastName}`}
											</span>
										</Link>
									</Col>
									<Col xs={6}>
											{localStorage.getItem('item_userId')!== id?
												<div className="itemE3">
													<Button
														onClick= {chat}
														type="button" 
														variant="success" 
														size="lg" 
														className="itemE4">
														Chat
													</Button>
												</div>
												:
												<div className="itemE3">
													<Button 
														className="mr-2"
														variant="success"
														onClick={()=>{ editItem()}}>
														Edit
													</Button>
													<Button 
														variant="danger"
														onClick={()=>{ deleteItem()}}>
														Delete
															
													</Button>
													
												</div> 
												
											}
									</Col>
								</Row>

								<p className="fnt1">
									PHP: {item.price}
								</p>

								<p className="fnt2">{item.title}</p>

								<p className="textWhiteSpace">
									{item.description}
								</p>

								<p>
									{`${itemOwner.town}, ${itemOwner.region}`} <br/>
									{itemOwner.country}
								</p>

								<p>Condition: {item.condition} </p>

								
							</div>
						</div>
					</Row>
				</Col>
			</Row>
			<Modal 
				size="lg"
				aria-labelledby="example-modal-sizes-title-lg"
				show={modalOpen} 
				onHide={closeModal}
				className="customized-modal-2">

				<Modal.Header closeButton>
		          <Modal.Title id="example-modal-sizes-title-lg">
		            Update Item
		          </Modal.Title>
		        </Modal.Header>
				<UpdateItem props={{closeModal, item, setItem, setPhotos}}/>
			</Modal>
		</React.Fragment>
	)
};
