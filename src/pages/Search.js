import React, { useState, useEffect, useCallback } from 'react';

import Advertisement from '../components/Advertisement';
import Categories from '../components/Categories';
import Item from '../components/Item';


import { useGlobalData } from '../UserProvider';

import { useRouteMatch, useHistory } from "react-router-dom";
import { Row, Col, Button } from 'react-bootstrap';

export default function Home () {

	
	let history = useHistory();

	
	const { refreshData2, setRefreshData2 } = useGlobalData();

	const [allItems, setAllItems] = useState([])
	// const [newItems, setNewItems] = useState([])

	const [db_Length, setDb_Length] = useState(0)
	// const [itemLimits, setItemLimits] = useState(5)

	const [loadMoreBtn, setLoadMoreBtn] = useState(false)

	const [start, setStart] = useState(0)
	const [last, setLast] = useState(0)
	
	
	let itemLimits = 18;  // number of items to be displayed
	let startingIndex = 0;
	let lastIndex = 0;

	const match = useRouteMatch({
		path: "/search/keyword=:keyword",
		strict: true,
		sensitive: true
	});

	
	useEffect(() => {
		
		let theKeyword = localStorage.getItem('keyword');
		let paramsKeyword = match.params.keyword;

		if(refreshData2) {
			
			setRefreshData2(false)
			
			history.push(`/search/keyword=${theKeyword}`)

		} else {
			

			if( String(paramsKeyword) !== 'null' || String(paramsKeyword) !== 'undefined' ) {

				fetch(`${process.env.REACT_APP_API_URL}/item/search/${paramsKeyword}`, {
					method: 'POST',
		            headers: {
		                'Content-Type' : 'application/json'
		            },
		            body: JSON.stringify({ 
		            	db_Length: (db_Length>itemLimits)? db_Length : 0,
		            	itemLimits: itemLimits,
		                startingIndex: startingIndex,
		                lastIndex: lastIndex,
		            })
		        })
			    .then(res => res.json())
		        .then(data => {
		        	localStorage.removeItem('keyword')
		        	// console.log(data)
		        	if(data.len>0){
		        		
						setAllItems(data.items)
						
						setDb_Length(data.len)

						if(data.len > itemLimits) {	setLoadMoreBtn(true) }

		        	}  else {

		        		setAllItems([])
		        		setDb_Length(0)
		        	}
		        })
			}
		}
		
	}, [refreshData2 , window.location.href])



	const loadMore = () => {
		const paramsKeyword = match.params.keyword;


		if(db_Length!==0) {
			if((startingIndex===0 && lastIndex===0) && (start===0 && last ===0) ) {

				lastIndex = lastIndex + itemLimits;
				startingIndex = lastIndex + itemLimits;
				
				setLast(lastIndex)
				setStart(startingIndex)
				
			} else {
				lastIndex = (last + itemLimits);
				startingIndex = (start + itemLimits);

				setLast(lastIndex)
				setStart(startingIndex)
			}
			
			fetch(`${process.env.REACT_APP_API_URL}/item/search/${paramsKeyword}`, {
				method: 'POST',
	            headers: {
	                'Content-Type' : 'application/json'
	            },
	            body: JSON.stringify({
	            	db_Length: db_Length,
	                startingIndex: startingIndex,
	                lastIndex: lastIndex,
	                itemLimits: itemLimits
	            })
			})
			.then(res => res.json())
	        .then(data => {
	        	
	        	if(data.len>0){
	        		
	        		// setNewItems(data.items)
	        		addNewItems(data.items)

						
					setDb_Length(data.len)

	        		if(startingIndex >= db_Length) {
						setLoadMoreBtn(false)
					} else {
						setLoadMoreBtn(true)
					}
	        		
	        	} else {
	        		setDb_Length(0)
	        	}
	        })
		} else {
			console.log('Opps! something went wrong!')
		}


	}

	// adding new items to render
	// useEffect(()=>{
	// 	let temp = [];
	// 	let cnt = 0;
	// 	let newItemsLength = newItems.length;
	// 	if(newItemsLength>0) {

	// 		newItems.map(element => {
	// 			cnt++
	// 			temp.push(element)

	// 			if(cnt>= newItemsLength) {
					
	// 				allItems.map(element=> {
	// 					temp.push(element)
	// 				})
	// 			}
	// 		})	
	// 	}
	// 	setAllItems(temp)

	// },[newItems])

	function addItem(item){
		
		setAllItems(previtems=> {
			return [item, ...previtems]
		})

	}

	const addNewItems = useCallback((newitems)=>{

		for(let item of newitems){
			addItem(item)
		}

	}, [])


	const itemsArray = allItems.map((element, idx) => {
		
		if(element.isActive){
			return {
				category: element.category,
				condition: element.condition,
				createdOn: element.createdOn,
				description: element.description,
				firstName: element.firstName,
				isActive: element.isActive,
				lastName: element.lastName,
				photos: element.photos,
				price: element.price,
				subCategory: element.subCategory,
				title: element.title,
				userId: element.userId,
				_id: element._id,
				num: idx
			}
		}
	})

	// from latest to oldest
	const sortedItems = itemsArray.sort((a,b) => {
		if (a.num < b.num) {
			return 1
			
		} else if (a.num > b.num) {
			return -1
			
		} else {
			return 0
		}
	})

	const arrangedItems = sortedItems.map(element => {
		if(element.isActive) {
			
			return (
				<Item key={element._id} dataProps={element}/>
			)
		}
	})


	
	return (
		<React.Fragment>
		
			<Row className="my-5 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0 ">
					<Row className="mb-4 mt-1 px-3" noGutters>
						<Col xs={12}>
							<span className="lgTxt">CATEGORIES</span>
						</Col>
					</Row>
					<Row noGutters>
						<Col xs={12}>
							<Categories/>
						</Col>
					</Row>
				</Col>
			</Row>
			<Row className="my-5 pb-4 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0 ">
					<Row className="mb-4 mt-1 px-3" noGutters>
						<Col xs={12}>
						{
							db_Length>1 ?
							<span className="lgTxt">Result: {db_Length} items found</span>
							:
								db_Length===0?
								<span className="lgTxt">Result: No item found</span>
								:
								<span className="lgTxt">Result: {db_Length} item found</span>
						}
							
						</Col>
					</Row>
					<Row noGutters>
						<Col xs={12}>
							<div className="homeDiv1">
								{arrangedItems}
							</div>
							<Row className="justify-content-center mt-3" noGutters>
							{loadMoreBtn?
								<React.Fragment>
									<Button 
										variant="warning"
										type="button"
										onClick={loadMore}
										className=" px-5 py-1">
											LOAD MORE
									</Button>
									
								</React.Fragment>
								:
								null
							}
							
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
		</React.Fragment>
	)
};


