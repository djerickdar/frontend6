import React, { useState, useEffect, useCallback } from 'react';

import Advertisement from '../components/Advertisement';
import Categories from '../components/Categories';
import Item from '../components/Item';


import { Row, Col, Button } from 'react-bootstrap';

export default function Home () {

	const [allItems, setAllItems] = useState([])

	// const [newItems, setNewItems] = useState([])

	const [db_Length, setDb_Length] = useState(0)
	
	const itemLimits = 18

	const [loadMoreBtn, setLoadMoreBtn] = useState(false)

	const [start, setStart] = useState(0)
	const [last, setLast] = useState(0)
	

	let startingIndex = 0;
	let lastIndex = 0; 


	// item limits set in the backend
	useEffect(()=>{
		localStorage.removeItem('item_userId')
		localStorage.removeItem('item_userFirstName')
		localStorage.removeItem('convoRefId')
		localStorage.removeItem('_itemId')
		localStorage.removeItem('prevCategory')
		localStorage.removeItem('keyword')
		
		fetch(`${process.env.REACT_APP_API_URL}/item/query/items_list`) 
		.then(res => res.json())
        .then(data => {
        	
        	if(data.items.length>0){

        		setAllItems(data.items)
        		// setCc(data.items)
        		setDb_Length(data.len)

        		if(data.len > itemLimits) {
        			setLoadMoreBtn(true)
        		}
        	} else {
        		setAllItems([])
        	}
        })

	},[])


	const loadMore = () => {

		if(db_Length!==0) {
			if((startingIndex===0 && lastIndex===0) && (start===0 && last ===0) ) {

				lastIndex = lastIndex + itemLimits;
				startingIndex = lastIndex + itemLimits;
				
				setLast(lastIndex)
				setStart(startingIndex)
				
			} else {
				lastIndex = (last + itemLimits);
				startingIndex = (start + itemLimits);

				setLast(lastIndex)
				setStart(startingIndex)
			}
			
			fetch(`${process.env.REACT_APP_API_URL}/item/query/version2_get_all_item`, {
				method: 'POST',
	            headers: {
	                'Content-Type' : 'application/json'
	            },
	            body: JSON.stringify({
	            	db_Length: db_Length,
	                startingIndex: startingIndex,
	                lastIndex: lastIndex,
	                itemLimits: itemLimits
	            })
			})
			.then(res => res.json())
	        .then(data => {
	        	if(data.length>0){
	        		
	        		// setNewItems(data)
	        		addNewItems(data)
	        		if(startingIndex >= db_Length) {
						setLoadMoreBtn(false)
					} else {
						setLoadMoreBtn(true)
					}
	        		
	        	} 
	        })
		} else {
			console.log('something went wrong!')
		}


	}

	function addItem(item){
		
		setAllItems(previtems=> {
			return [item, ...previtems]
		})

	}

	const addNewItems = useCallback((newitems)=>{

		for(let item of newitems){
			addItem(item)
		}

	}, [])


	const itemsArray = allItems.map((element, idx) => {
		
		// if(element.isActive){
			return {
				category: element.category,
				condition: element.condition,
				createdOn: element.createdOn,
				description: element.description,
				firstName: element.firstName,
				isActive: element.isActive,
				lastName: element.lastName,
				photos: element.photos,
				price: element.price,
				subCategory: element.subCategory,
				title: element.title,
				userId: element.userId,
				_id: element._id,
				num: idx
			}
		// }
	})

	// from latest to oldest
	const sortedItems = itemsArray.sort((a,b) => {
		if (a.num < b.num) {
			return 1
			
		} else if (a.num > b.num) {
			return -1
			
		} else {
			return 0
		}
	})

	const arrangedItems = sortedItems.map(element => {
		// if(element.isActive) {
			
			return (
				<Item key={element._id} dataProps={element}/>
			)
		// }
	})
	
	return (
		<React.Fragment>
		
			<Row className="my-3 bgWhite" noGutters>
				<Col xs={12} className="p-0 ">
					<Advertisement/>		
				</Col>
			</Row>

			<Row className="my-5 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0 ">
					<Row className="mb-4 mt-1 px-3" noGutters>
						<Col xs={12}>
							<span className="lgTxt">CATEGORIES</span>
						</Col>
					</Row>
					<Row noGutters>
						<Col xs={12}>
							<Categories/>
						</Col>
					</Row>
				</Col>
			</Row>
			<Row className="my-5 pb-4 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0 ">
					<Row className="mb-4 mt-1 px-3" noGutters>
						<Col xs={12}>
							<span className="lgTxt">FRESH ITEMS</span>
						</Col>
					</Row>
					<Row noGutters>
						<Col xs={12}>
							<div className="homeDiv1">
								{arrangedItems}
							</div>
							<Row className="justify-content-center mt-3" noGutters>
							{loadMoreBtn?
								<React.Fragment>
									<Button 
										variant="warning"
										type="button"
										onClick={loadMore}
										className=" px-5 py-1">
											LOAD MORE
									</Button>
									
								</React.Fragment>
								:
								null
							}
							
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
		</React.Fragment>
	)
};









