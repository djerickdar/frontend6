import React, {useContext, useState } from 'react'

import towns from './data/towns';
import regions from './data/regions';
import countries from './data/countries';

const UserContext = React.createContext();

export function useGlobalData() {
	return useContext(UserContext)
}

export function UserProvider({children}) {

	const [url, setUrl ] = useState('');
	const [url2, setUrl2 ] = useState('');
	// const [myToken, setMyToken] = useState(localStorage.getItem('token'))

	const [refreshData, setRefreshData] = useState(false)
	const [refreshData2, setRefreshData2] = useState(false)

	// this is for login Modal
	const [show1, setShow1] = useState(false)


	const [user, setUser] = useState({

		id: null,
		isActive: null,
		isAdmin: null,
		photo: null,
		firstName: null,
		lastName: null,	
		email: null,
		country: null,
		region: null,
		town: null,
		mobileNo: null,
		createdOn: null,
		loginType: null
	});


	const unsetUser = () => {
		
		localStorage.clear();
		setUser({
			id: null,
			isActive: null,
			isAdmin: null,
			photo: null,
			firstName: null,
			lastName: null,	
			email: null,
			country: null,
			region: null,
			town: null,
			mobileNo: null,
			createdOn: null,
			loginType: null
		})
	}

	function addressName(addr){
		let dCountry = null;
		let dTown = null;
		let dRegion = null;
		
		dTown = towns.find((value)=>{return value.town===addr.town})
		dRegion = regions.find((value)=>{return value.region===addr.region})
		dCountry = countries.find((value)=>{return value.country===addr.country})

		return {
			town: dTown ? dTown.townN : '', 
			region: dRegion? dRegion.regionN : '', 
			country: dCountry ? dCountry.countryN : ''
		}
	}

	// useEffect(()=> {
		
		// const storage = localStorage.getItem('userId')

		// if( String(storage)=== 'undefined' || String(storage) ==='null' ) {

		// 	console.log('storage true:', storage, 'typeof', typeof storage)
		// }else {
		// 	console.log('storage false:',  storage, 'typeof', typeof storage)
		// }

	// })


	
	const value = {user, setUser, unsetUser, url, setUrl, url2, setUrl2, refreshData, setRefreshData, refreshData2, setRefreshData2, show1, setShow1, addressName}
	return (
		<UserContext.Provider value={value}>
			{children}
		</UserContext.Provider>
	)
}

// prvious file name: UserContext.js to UserProvider