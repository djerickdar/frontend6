import React, {useContext, useState, useCallback, useEffect, useMemo } from 'react'

import useLocalStorage from '../components/hooks/useLocalStorage'

import { useSocket } from './SocketProvider';

import {v4 as uuidV4} from 'uuid'
const msgRefId =  uuidV4();

const ConversationsContext = React.createContext()

export function useConversations() {
	return useContext(ConversationsContext)
}


export function ConversationsProvider({children}) {

	const [conversations, setConversations] = useLocalStorage('conversations', [])
	const [my, setMy] = useLocalStorage('my')
	// const [uid, setuId] = useLocalStorage('id')
	// const [theUserName, setTheUserName] = useLocalStorage('theUserName')
	
	const [selectedConvoId, setSelectedConvoId] = useState('')


	let id = localStorage.getItem('userId')

	const socket = useSocket()

  	let indexDate = dateIndex();


	function createConversation (params) {
		
		setConversations(prevConversations => {


			const data = {
				itemId: params.itemId,
				item:params.item,
				convoRefId: params.convoRefId,
				recipients: {id:params.id, name:params.name},
				messages:[],
				indexDate: indexDate,
				savedtoDb: false
			}
			let madeChange = false;

			// 
			const convos = prevConversations.map(conversation=>{
				if(conversation.convoRefId=== params.convoRefId){
					madeChange =true;

					return {...conversation, indexDate:indexDate}
				}

				return conversation
			})

			if(madeChange){
				setSelectedConvoId(params.convoRefId)
				return convos

			}else{
				setSelectedConvoId(params.convoRefId)
				return [...prevConversations, data ]	
			}

		})
	}


	const addMessageToConversation = useCallback(({ data }) => {
		

		setConversations( prevConversations => {

			let madeChange = false;

			const _data = {
				itemId: data.itemId,
				item: data.item,
				convoRefId:data.convoRefId,
				recipients: {
					id:data.recipients.id, 
					name:data.recipients.name
				},
				messages: [data.messages],
				indexDate:data.indexDate
			}


			let indexDate = data.indexDate;

			const newMessage = {
				sender:data.messages.sender, 
				text:data.messages.text,
				msgRefId: data.messages.msgRefId,
				msgSent: data.messages.msgSent
			}

			function findMessage(params){return params.msgRefId === data.messages.msgRefId}


			const newConversations = prevConversations.map(conversation => {

				if(conversation.convoRefId === data.convoRefId && typeof data.messages === 'object') {

					madeChange = true

					const msg = conversation.messages.find(findMessage)

					if(msg){

						const _msgs = conversation.messages.map(message => {

							if(message.msgRefId===data.messages.msgRefId){

								return {...message, msgSent: data.messages.msgSent}			
							}

							return message
						})

						return { ...conversation, item: data.item, indexDate, savedtoDb: true, messages: _msgs }


					}else{

						return { ...conversation,item: data.item, indexDate, messages: [...conversation.messages, newMessage] }	
					}
				}
				return conversation
			})


			if(madeChange) {

				return newConversations
			} else {

				return [ ...prevConversations, _data ]
			}
		})
	}, [setConversations])



	useEffect(() => {
		
    	if (socket == null) return

    	socket.on('receive-message', addMessageToConversation)

    	return () => socket.off('receive-message')
  	}, [socket, addMessageToConversation])



	function sendMessage(params) {
	
			const data = { 
				itemId: params.itemId,
				convoRefId: params.convoRefId,
				recipients: params.recipients,
				messages:{ 
					sender: id, 
					text: params.text,
					msgRefId: msgRefId+'t'+indexDate,
					msgSent: false
				},
				indexDate: indexDate,
				savedtoDb: false
			}

		// socket.emit('send-message', { recipients, text })

		socket.emit('send-message', { data })

		addMessageToConversation({data})
	}

	const getAllConvos = ( theData ) => {
	
		setConversations( prevConversations => {

			function findConvo(params){return params.convoRefId === theData.convoRefId}
				
			let convo = prevConversations.find(findConvo)

			if(prevConversations.length>0) {
			
				if(String(convo)==='undefined' && String(convo)!=='null'){
					return [ ...prevConversations, theData ]
				}
						
			} else {

				return [ ...prevConversations, theData ]
			}
		})
	}

	function getAllConversations(convos){

		id = localStorage.getItem('userId');

		let names = [];
		let items =[];

		let x = 0;

		for (let c of convos) {
			
	    	fetch(`${process.env.REACT_APP_API_URL}/users/getconversations`, {
	            method: 'POST',
	            headers: {
	                'Content-Type' : 'application/json'
	            },
	            body: JSON.stringify({ convoRefId:c.recipients })
	        })
	        .then(res => res.json())
	        .then(data1 => {
	        	if(data1){
		        	fetch(`${process.env.REACT_APP_API_URL}/item/itemdetails/${c.itemId}`) 
					.then(res => res.json())
			        .then(data2 => {
			        	
			        		names.push(data1)
			        		items.push(data2)
			        		x =x +1 ;
			        		
			        		if(x=== convos.length && x>0){
			        			

					        	convos.forEach( d =>{
									function findName(params){return params.id === d.recipients}
									let name = names.find(findName)

									function findItem(params){return params._id === d.itemId}
									let item = items.find(findItem)

									let theData = {
										itemId: d.itemId,
										item: item,
										convoRefId: d.convoRefId,
										recipients: {id: d.recipients, name:name.firstName},
										savedtoDb: d.savedtoDb,
										indexDate: d.indexDate,
										messages: d.messages
									}
									
									getAllConvos(theData)
								})
					        }

			        	
			        })
				}else{ console.log('[ERR3]:Opps!, Something went wrong.') }
	
	        })   
	    }   
	}

	const sortedItems = conversations.sort((a,b) => {
		if (a.indexDate < b.indexDate) {
			return 1
			
		} else if (a.indexDate > b.indexDate) {
			return  -1
			
		} else {
			return 0
		}
	})

	const formattedConversations = sortedItems.map((conversation, index) => {

		const recipients = conversation.recipients;

		const messages = conversation.messages.map(message => {
			
			const fromMe = id === message.sender
			
			return {...message, fromMe}
		})

		const selected = conversation.convoRefId === selectedConvoId;


		return {...conversation, messages, recipients, selected}
	})

	// convert number to string with coma
	const numComma = (num) => {
		console.log('numComma num typeof:', typeof num)
		const n = num.replace(/,/g, "")
		let myNUm = parseFloat(n);
		let withCommas = Number(myNUm.toFixed(2)).toLocaleString('en');
		
		return withCommas
	}

	const deleteConvo =(params)=>{
		

		function removeConversation(par){
			setConversations([])
			getAllConversations(par)
			
		}

		function removeConversation2(convoId){
			
			setConversations(prevConversations=>{

				let temp = [];
				prevConversations.forEach(convo=> {
					if(convo.savedtoDb || convo.convoRefId!== convoId){
						 temp.push(convo)
					}
				})

				
				return temp
			})
			
		}

		if(params.savedtoDb){
			fetch(`${process.env.REACT_APP_API_URL}/users/delete-conversation`, {
					method: 'DELETE',
				headers: {
					'Content-Type' : 'application/json',
					'Authorization' : `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					convoRefId:params.id
				})
	        })
	        .then(res => res.json())
	        .then(data => {
				if(data){
					removeConversation(data.conversations)
				}
			})
		}else{
			removeConversation2(params.id)
		}
	}
	

	const theConvoIndex = useMemo(()=> {

		function indexFinder(params) {
			
		  return params.convoRefId === selectedConvoId
		}

		const myIndex = formattedConversations.findIndex(indexFinder);

		return myIndex
		
	},[selectedConvoId, conversations])


	
	const value = {
		conversations:formattedConversations, 									// Conversation.js

		selectedConversation:formattedConversations[theConvoIndex],	// OpenConversation.js

		sendMessage,															// OpenConversation.js

		createConversation,													// NewConversationModal.js

		indexDate,

		selectConvoId: setSelectedConvoId,
		
		getAllConversations,
		
		id, numComma, deleteConvo, setMy, my
	} 
	return (
		<ConversationsContext.Provider value={value}>
			{children}
		</ConversationsContext.Provider>
	)
}

function dateIndex() {
	let t = new Date();
  		
  		let year = ''+t.getFullYear()

  		let month = t.getMonth()<10 && t.getMonth() > -1 ? '0'+t.getMonth() : ''+t.getMonth();
  
		let date = t.getDate()<10 && t.getDate() > -1 ? '0'+t.getDate() : ''+t.getDate();

		let hours = t.getHours()<10 && t.getHours() > -1 ? '0'+t.getHours() : ''+t.getHours();

		let minutes = t.getMinutes()<10 && t.getMinutes() > -1 ? '0'+t.getMinutes() : ''+t.getMinutes();

		let seconds = t.getSeconds()<10 && t.getSeconds() > -1 ? '0'+t.getSeconds() : ''+t.getSeconds();

		let milliseconds = '';

	  		if(t.getMilliseconds()<10 && t.getMilliseconds() > -1) {
	  			
	  			milliseconds = '00'+t.getMilliseconds();
	  		} else if(t.getMilliseconds()<100 && t.getMilliseconds() > 9) {

	  			milliseconds = '0'+t.getMilliseconds();
	  		}else {

	  			milliseconds = ''+t.getMilliseconds();
	  		}

	  	let fullDate = year+month+date+hours+minutes+seconds+milliseconds;
	  	return parseFloat(fullDate);
}
