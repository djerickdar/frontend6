import React, { useContext, useEffect, useState } from 'react'
import io from 'socket.io-client'

const SocketContext = React.createContext()

/*
  export const SocketProvider = SocketContext.Provider

  export default SocketContext

  let's assume that the file Name is SocketContext
 */


/*
  import { SocketProvider } from './SocketCOntext'

  function Chat () {

    const [id, setId] = useLocalStorage('id')

    return (
      <SocketProvider value={{  }}>

          <Dashboard id={id} />

      </SocketProvider>
    )
  }
 
*/

export function useSocket() {

  return useContext(SocketContext)
}


/*
  import { useSocket } from './SocketProvider';

  const socket = useContext( React.createContext() )

  const socket = useSocket()
*/

// const SocketContext = React.createContext()

// export const SocketProvider = SocketContext.Provider

export function SocketProvider({ id, children }) {
  const [socket, setSocket] = useState()

  useEffect(() => {
    const newSocket = io(
      'http://localhost:4000',
      { query: { id } }
    )
    setSocket(newSocket)

    // close the old socket and create a new one
    return () => newSocket.close()
  }, [id])

  return (
    <SocketContext.Provider value={socket}>
      {children}
    </SocketContext.Provider>
  )
}

// export default SocketContext

// 1.29.27

//