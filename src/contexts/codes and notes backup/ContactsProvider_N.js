import React, {useContext} from 'react'
import useLocalStorage from '../components/hooks/useLocalStorage'

const ContactsContext = React.createContext()



export function useContacts() {
	return useContext(ContactsContext)
}


// as component
export function ContactsProvider({children}) {

	const [contacts, setContacts] = useLocalStorage('contacts', [])

	
	function createContact (id, name) {
		
		// contacts = prevContacts
		
		// actual: setContacts( 'contacts' => { .. } )

		setContacts(prevContacts => {
			

			return [...prevContacts, {id, name}]
		})
	}

	return (
		<ContactsContext.Provider value={{contacts, createContact}}>
			{children}
		</ContactsContext.Provider>
	)
}



// -------------------------

// const { createContact } = useContacts()


// -------------------------

/*
import React from 'react';

const UserContext = React.createContext();

// as component
export const UserProvider = UserContext.Provider;


export default UserContext;*/