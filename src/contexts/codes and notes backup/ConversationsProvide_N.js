import React, {useContext, useState, useCallback, useEffect	} from 'react'
import useLocalStorage from '../components/hooks/useLocalStorage'
import { useContacts } from './ContactsProvider'

import { useSocket } from './SocketProvider';

const ConversationsContext = React.createContext()

export function useConversations() {
	return useContext(ConversationsContext)
}

// as component
export function ConversationsProvider({id, children}) {

	// useLocalStorage variable 'conversations' is reference to 'formattedConversations', it's used to map conversations

	// Conversation.js -> conversations : formattedConversations
	// OpenConversation.js -> selectedConversation : formattedConversations[selectedConversationIndex]

	//const formattedConversations = conversations.map ( ... )

	const [conversations, setConversations] = useLocalStorage('conversations', [])

	// linked to: Conversation.js -> setSelectedConversationIndex
	
	const [selectedConversationIndex, setSelectedConversationIndex] = useState(0)

	const {contacts} = useContacts()

	const socket = useSocket()

	/*
		NewConversationModal.js

		recipients = [ <contactIds>, <contactIds>, <contactIds> ]
		recipients - id's selected earlier - ["8", "9"]
		
		creeating group chat or one-on-one chat
	*/

	// conversation = [
	// 	{
			// messages: [ {sender:"sendeId", text:"text" }, {sender:"sendeId", text:"text" } ]

			// messages: [ ] // initial value
		// },
		// {
			// recipients: ["recieversId"]

			// recipients: {id:"recieversId" , name:"recieversName"} // modified
	// 	}
	// ]

	/* recipients: { id:"recieversId" , name:"recieversName" } */
	function createConversation (recipients) {
		
		setConversations(prevConversations => {
			return [...prevConversations, { recipients, messages:[] }]
		})
	}
/*	
	conversations = [	
			{	recipients: ["1", "2"],	messages: [] },

			{	recipients: ["2"], 		messages: [] } ] 


	sendMessage(recipients, text) -> OpenConversation.js

	sendMessage( selectedConversation.recipients.map(r => r.id), text )

			   ( formattedConversations[selectedConversationIndex] ) =( [ "1", "2" ], text )

	addMessageToConversation({recipients, text, sender:id})


	this function changes every single time we re-render our component, that's why we wrap it up with 
	useCallback for us to avoid that, with this useCallback it listens to setConversation, and everytime it 
	changes the useCallback re-renders
*/
	const addMessageToConversation = useCallback(({ recipients, text, sender, success }) => {

		setConversations(prevConversations => {
			
			let madeChange = false

			const newMessage = {sender, text}

			/*
				prevConversations = [
				{	recipients: ["1", "2"],	messages: [] },

				{	recipients: ["2"], 		messages: [] } ] 

				Dito, mina-map niya ang lahat ng previous conversations at 
				parang kinukumpara niya yung mga recipients from previous convo to current conversation
				parang chinicheck niya kung same pa rin ba yung mga taong nag uusap 
				(arrayEqually), kung true naman then i aadd niya yung bagong message, else, pg hindi, as is.

				ang problema, kung mg create ka ng bagong conversation na same ang rcipient sa prviouse one, mag sesend yung message mo sa parehang conversation group
			*/
			const newConversations = prevConversations.map(conversation => {

				// if(arrayEqually(conversation.recipients, recipients)) {
				if(conversation.recipients === recipients) {

					madeChange = true

					return { ...conversation, messages: [...conversation.messages, newMessage] }
				}

				return conversation
			})

			if(madeChange) {
				
				return newConversations
			} else {

				return [ ...prevConversations,{ recipients, messages: [newMessage] } ]
			}
		})
	}, [setConversations])

	// handle the recieved message | socket.on('receive-message', addMessageToConversation)
	useEffect(() => {
		// console.log('socket: ', socket)
		// check if the socket exist
    	if (socket == null) return

    	/*
    		what happened here is we call the addMessageToConversation function because in our server we are passing down the recipeints, the sender and the text which is exactly what that 
    		function takes ( addMessageToConversation )  
    	*/
    	socket.on('receive-message', addMessageToConversation)

    	/*  this will take the socket and remove that event listener */

    	return () => socket.off('receive-message')
  	}, [socket, addMessageToConversation])


/*									    (   ["1", "2"], text, sender:id )
	function addMessageToConversation ( { recipients, text, sender } ) {

		setConversations(prevConversations => {
			
			let madeChange = false

			const newMessage = {sender, text}

			const newConversations = prevConversations.map(conversation => {

				if(arrayEqually(conversation.recipients, recipients)) {

					madeChange = true
					
					return { ...conversation, messages: [...conversation.messages, newMessage] }
				}

				return conversation
			})

			if(madeChange) {

				return newConversations
			} else {
				
				return [ ...prevConversations,{ recipients, messages: [newMessage] } ]
			}
		})
	}
*/

	// OpenConversation.js
	function sendMessage(recipients, text) {
		
		// send a msg to all of diff clients

		socket.emit('send-message', { recipients, text })

		// if you comment this out, hindi mo makikita yung message na sinend mo,

		addMessageToConversation({recipients, text, sender:id, success:true})
	}
	/*	
		conversations = [	
				{	recipients: ["1", "2"],	messages: [ { sender:"id", text:"some-text" } ] },

				{	recipients: ["2"], 		messages: [] } 
			] 
		
		Conversation.js -> conversations : formattedConversations

		OpenConversation.js -> selectedConversation : formattedConversations[selectedConversationIndex]

		conversations = [	
			{	recipients: ["1", "2"],	messages: [] },

			{	recipients: ["2"], 		messages: [] } ] 
	*/

	const formattedConversations = conversations.map((conversation, index) => {
		
		/*
			return the recipients details (typeof array)

			conparing recipients to saved contacts and returning it
		
		*/
		// const recipients = conversation.recipients.map(recipient => {

			/*const contact = contacts.find(contact => {
				return contact.id === recipient
			})*/

			// if contact name is found then return the name, else return rcipient id
			/*const name = (contact && contact.name) || recipient*/

		// 	return recipient
		// })

		const recipients = conversation.recipients;

		const messages = conversation.messages.map(message => {
			
			// const contact = contacts.find(contact => {
			// 	return contact.id === message.sender
			// })
			
			// const name = (contact && contact.name) || message.sender
			
			const fromMe = id === message.sender 

			return {message, senderName: '', fromMe}
		})

		const selected = index === selectedConversationIndex

		return {...conversation, messages, recipients, selected}
	})

	/*
		conversation = [
			{
				messages: [ {sender:"sendeId", text:"text" }, {sender:"sendeId", text:"text" } ]

				messages: [ ]
			},
			{
				recipients: ["recieversId"]

				recipients: {id:"recieversId" , name:"recieversName"}
			}
		]
		formattedConversations = [
			{
				messages: [
					{
						fromMe: true
						sender: "839b235a-b869-4c37-8acb-a6dc77b33755"
						senderName: "839b235a-b869-4c37-8acb-a6dc77b33755"
						text: "hi"
					},	
					{
						fromMe: true
						sender: "839b235a-b869-4c37-8acb-a6dc77b33755"
						senderName: "839b235a-b869-4c37-8acb-a6dc77b33755"
						text: "hello↵"
					}
				],
				recipients: [  { id:"2", name:"2" } ],
				selected: true
			},
			{
				messages: [],
				recipients: [ { id:"1", name:"1" } ],
				selected: false
			}
		]
	*/
	const value = {
		conversations:formattedConversations, 									// Conversation.js

		selectedConversation:formattedConversations[selectedConversationIndex],	// OpenConversation.js

		sendMessage,															// OpenConversation.js

		selectConversationIndex: setSelectedConversationIndex,  				// Conversation.js

		createConversation,														// NewConversationModal.js

		convo: conversations
	} 
	return (
		<ConversationsContext.Provider value={value}>
			{children}
		</ConversationsContext.Provider>
	)
}

//	return true if every element of the array meets the condition
// function arrayEqually(a, b) {
// 	if(a.length !== b.length) return false

// 	a.sort()
// 	b.sort()

// 	return a.every((element, index) => {
// 		return element === b[index]
// 	})
// }
// -------------------------

// const { createContact } = useContacts()


// -------------------------

/*
import React from 'react';

const UserContext = React.createContext();

// as component
export const UserProvider = UserContext.Provider;


export default UserContext;*/


/*0: {
	messages: [
		{
			fromMe: true
			sender: "839b235a-b869-4c37-8acb-a6dc77b33755"
			senderName: "839b235a-b869-4c37-8acb-a6dc77b33755"
			text: "hi"
		},	
		{
			fromMe: true
			sender: "839b235a-b869-4c37-8acb-a6dc77b33755"
			senderName: "839b235a-b869-4c37-8acb-a6dc77b33755"
			text: "hello↵"
		}
	],	
	
	recipients:[ 
		{
			id: "7", name: "7"
		} 
	],

	selected: true
}*/