export default [ 

{ 
			category : 'cars_and_properties' ,  
			subCategory : 'car_parts_and_accessories' , 
			title :  'Car Parts and Accessories' 
		},
		{ 
			category : 'cars_and_properties' ,  
			subCategory : 'motobike' , 
			title :  'Motorbike' 
		},
		{ 
			category : 'cars_and_properties' ,  
			subCategory : 'for_rent_property' , 
			title :  'For rent property' 
		},
		{ 
			category : 'cars_and_properties' ,  
			subCategory : 'others' , 
			title :  'Others' 
		},
		{ 
			category : 'mobile_and_electronics' ,  
			subCategory : 'mobile_phones' , 
			title :  'Mobile Phones' 
		},
		{ 
			category : 'mobile_and_electronics' ,  
			subCategory : 'computers' , 
			title :  'Computers' 
		},
		{ 
			category : 'mobile_and_electronics' ,  
			subCategory : 'gadgets' , 
			title :  'Gadgets' 
		},
		{ 
			category : 'mobile_and_electronics' ,  
			subCategory : 'home_appliances' , 
			title :  'Home Appliances' 
		},
		{ 
			category : 'mobile_and_electronics' ,  
			subCategory : 'others' , 
			title :  'Others' 
		},
		{ 
			category : 'jobs_and_services' ,  
			subCategory : 'jobs_and_oppurtinities' , 
			title :  'Jobs and Oppurtinities' 
		},
		{ 
			category : 'jobs_and_services' ,  
			subCategory : 'home_services' , 
			title :  'Home Services' 
		},
		{ 
			category : 'jobs_and_services' ,  
			subCategory : 'business_services' , 
			title :  'Business Services' 
		},
		{ 
			category : 'jobs_and_services' ,  
			subCategory : 'car_rentals' , 
			title :  'Car Rentals' 
		},
		{ 
			category : 'jobs_and_services' ,  
			subCategory : 'others' , 
			title :  'Others' 
		},
		{ 
			category : 'home_and_living' ,  
			subCategory : 'furnitures_and_home_living' , 
			title :  'Furnitures and Home Living' 
		},
		{ 
			category : 'home_and_living' ,  
			subCategory : 'babies_and_kids' , 
			title :  'Babies and Kids' 
		},
		{ 
			category : 'home_and_living' ,  
			subCategory : 'health_and_nutrition' , 
			title :  'Health and Nutrition' 
		},
		{ 
			category : 'home_and_living' ,  
			subCategory : 'food_and_drinks' , 
			title :  'Food and Drinks' 
		},
		{ 
			category : 'home_and_living' ,  
			subCategory : 'pet_supplies' , 
			title :  'Pet Supplies' 
		},
		{ 
			category : 'home_and_living' ,  
			subCategory : 'others' , 
			title :  'Others' 
		},
		{ 
			category : 'fashion' ,  
			subCategory : 'mens_fashion' , 
			title :  "Men's Fashion" 
		},
		{ 
			category : 'fashion' ,  
			subCategory : 'womens_fasion' , 
			title :  "Women's Fasion" 
		},
		{ 
			category : 'fashion' ,  
			subCategory : 'others' , 
			title :  'Others' 
		},
		{ 
			category : 'hobbies_and_games' ,  
			subCategory : 'toys_and_games' , 
			title :  'Toys and Games' 
		},
		{ 
			category : 'hobbies_and_games' ,  
			subCategory : 'music_and_media' , 
			title :  'Music and Media' 
		},
		{ 
			category : 'hobbies_and_games' ,  
			subCategory : 'books_and_magazines' , 
			title :  'Books and Magazines' 
		},
		{ 
			category : 'hobbies_and_games' ,  
			subCategory : 'travel' , 
			title :  'Travel' 
		},
		{ 
			category : 'hobbies_and_games' ,  
			subCategory : 'others' , 
			title :  'Others' 
		},
		{ 
			category : 'others' ,  
			subCategory : 'commercial_and_industial' , 
			title :  'Commercial and Industial' 
		},
		{ 
			category : 'others' ,  
			subCategory : 'community' , 
			title :  'Community' 
		},
		{ 
			category : 'others' ,  
			subCategory : 'others' , 
			title :  'Others' 
		}
]