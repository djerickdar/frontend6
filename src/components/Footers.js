import React from 'react';
import { Row, Col, } from 'react-bootstrap';


export default function Footers () {


	return (
		<React.Fragment> 
			<Row className="mt-5 footer shadow d-flex" noGutters>
				<Col className="pl-4 py-4" xs={12} md={6}>
					<Row className="justify-content-center justify-content-md-start " noGutters>
					
						<span>Copyright © 2020 Star Shop.  All rights reserved</span>
					
					</Row>
				</Col>
				<Col className="pt-0 pt-md-4 pb-4" xs={12} md={6}>
					<Row className="justify-content-center justify-content-md-end " noGutters>
					
						<span className="mx-3">About</span>
						<span className="mx-3">Privacy</span>
						<span className="mx-3">Terms</span>
						<span className="mx-3">Help</span>
					
					</Row>
				</Col>
			</Row>
		</React.Fragment>
	)
}