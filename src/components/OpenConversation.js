import React, {useState, useCallback} from 'react'
import {Form, InputGroup, Button, Image, Alert } from 'react-bootstrap'
import { useConversations } from '../contexts/ConversationsProvider'

export default function OpenCOnversation() {

	const [text ,setText] = useState('')
	
	const setRef = useCallback(node => {
		if(node) {
			node.scrollIntoView({smooth:true})	
		}
	},[])

	const {sendMessage, selectedConversation, indexDate, numComma} = useConversations()

	function handleSubmit(e) {
		e.preventDefault()

		const data = {
			itemId: selectedConversation.itemId,
			item: selectedConversation.item,
			convoRefId:selectedConversation.convoRefId,
			recipients:{
				id:selectedConversation.recipients.id, 
				name: selectedConversation.recipients.name
			},
			text: text,
			indexDate: indexDate
		}

		sendMessage(data)
		setText('')
	

	}

	return (
		<div style={{width: '600px'}} className="d-flex flex-column border">
			
			<div style={{width:'100%', height:'90px'}} className="d-flex p-1 border">
				{
					selectedConversation.item !==false && String(selectedConversation.item)!=='null'
					&& String(selectedConversation.item) !== 'undefined' ?
					<React.Fragment>
					<div style={{width:'80px', height:'80px'}}>
						<Image
							style={{width:'79px', height:'79px', objectFit: 'scaleDown'}}
							src={selectedConversation.item.photos[0].url}/>
					</div>
					<div className="pt-1 pl-2">
						<p>{selectedConversation.item.title}</p>
						<span style={{fontWeight: '600', fontSize:'18px'}}>
							PHP {numComma(selectedConversation.item.price)}
						</span>
					</div>	
					</React.Fragment>
					:
					null
				}
				
			</div>
			<div className="flex-grow-1 overflow-auto">

				
				<div className="d-flex flex-column align-items-start justify-content-end px-3">
				
					{selectedConversation.messages.map((message, index) => {
						const lastMessage = selectedConversation.messages.length - 1 === index
						return (
							<div
								ref={lastMessage? setRef : null }
								key={index}
								className={`my-1 d-flex flex-column ${message.fromMe ? 'align-self-end  align-items-end' : 'align-items-start'}`}
							>

								<div className={`rounded px-2 py-1 ${message.fromMe ? 'bg-primary text-white' : 'border'}`}>
									{message.text}
								</div>
								
								<div className={`text-muted small ${message.fromMe ? 'text-right' : ''} `}>
									{/*message.fromMe ? 'You' : message.senderName*/}
									{ message.fromMe? message.msgSent ? 'Sent' : 'Sending..' : ''}
								</div>

							</div>
						)
					})}
				</div>
			</div>
			<Form onSubmit={handleSubmit}>
				<Form.Group className="m-2">
					<InputGroup>
						<Form.Control
							as="textarea"
							required
							value={text}
							onChange={e => setText(e.target.value)}
							style={{height: '55px', resize: 'none'}}

						/>
						<InputGroup.Append>
							<Button variant="warning" type="submit">Send</Button>
						</InputGroup.Append>
					</InputGroup>
				</Form.Group>
			</Form>
			
		</div>
	)
}