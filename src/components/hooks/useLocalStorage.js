import {useEffect, useState} from 'react'

const PREFIX = 'ecommerce-app-'

export default function useLocalStorage(key, initialValue) {

	const preFixedKey = PREFIX + key;
	
	const [value , setValue] = useState(()=> {
		

		const jsonValue = localStorage.getItem(preFixedKey)
		
		if (jsonValue != null) {
			
			return JSON.parse(jsonValue)
		}
		
		if(typeof initialValue === 'function') {
			
			return initialValue()
		} else {
			
			return initialValue
		}
	}) 

	useEffect(()=> {
		
		const myId = localStorage.getItem('userId');

	
		if( String(myId)!== 'undefined' && String(myId) !== 'null' ) {


			localStorage.setItem(preFixedKey, JSON.stringify(value))
			
			
		} 

	},[preFixedKey, value])

	return [value, setValue]
}

