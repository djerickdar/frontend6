import React, {useState} from 'react'

import {Tab, Nav, Button, Modal} from 'react-bootstrap'

import Conversations from './Conversations'


const CONVERSATIONS_KEY = 'conversations'
const CONTACTS_KEY = 'contacts'

export default function Sidebar({id}) {
	
	const [activeKey, setActiveKey] = useState(CONVERSATIONS_KEY)
	const [modalOpen, setModalOpen] = useState(false)

	const conversationsOpen = activeKey === CONVERSATIONS_KEY

	function closeModal() {
		setModalOpen(false)
	}

	return (
		<div style={{width: '270px'}} className="d-flex flex-column"> 
			<Tab.Container activeKey={activeKey} onSelect={setActiveKey}>
				
				<Nav variant='tabs' className="justify-content-center mb-1">
					<Nav.Item>
						<Nav.Link eventKey={CONVERSATIONS_KEY}>Inbox </Nav.Link>
					</Nav.Item>
					
				</Nav>

				<Tab.Content 
					className="border-right overflow-auto flex-grow-1" 
					>
					
					<Tab.Pane eventKey={CONVERSATIONS_KEY} className="pr-1 pt-1">
						<Conversations/>
					</Tab.Pane>
					
				</Tab.Content>

			</Tab.Container>

		</div>
	)
}