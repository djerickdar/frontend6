import React from 'react';

import { Image } from 'react-bootstrap';


export default function Advertisement () {

	const src = {
		a: '/1.png',
		b: '/2.png',
		c: '/3.png'
	}
	
	return (
		<div>
			<Image
				className="d-block w-100"
				src={src.a}
				alt="First slide"
			/>
		</div>
		
	)
};

