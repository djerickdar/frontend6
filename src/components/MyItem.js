import React, { useEffect, useState } from 'react';

import {Nav, Row, Col, Image} from 'react-bootstrap';

// import { RiDeleteBin6Line } from "react-icons/ri";

import moment from 'moment'


export default function MyItem ({dataPropItem}) {
	

	const { condition, createdOn, photos, price, title, userId, _id } = dataPropItem;
	
	const [firstPic, setFirstPic] = useState('')

	useEffect(()=>{
		
		if(photos.length>0) {

			let tempurl = '';

			photos.forEach((element,idx) => {
				if(idx===0){tempurl=element.url}else{return null}
			})

			setFirstPic(tempurl)
		}
	}, [photos])

	const numComma = (num) => {
		let withCommas = Number(parseFloat(num).toFixed(2)).toLocaleString('en');

		return withCommas
	}



	return (
		<React.Fragment>
		<Nav.Link href={`/item/${userId}/${_id}`} className="_navlinkReset">
		{/*<Nav.Link href={`/item/${_id}`} className="_navlinkReset">*/}
			<div className="itemDiv1 borderGrey shadow-sm">
					<Image className="itemImg" src={`${firstPic}`}/>
				<Col xs="auto" className="p-0">
					<Row className="mt-2 borderTop" noGutters>
						<Col className="px-2">
							<span className="itemTitle">{title}</span><br/>
							<span>{condition}</span><br/>
							<span>{moment(createdOn).format('MMM DD, YYYY')}</span>
						</Col>
					</Row>
					<Row className="mb-3" noGutters>
						<Col xs="9" className="pl-2 p-0">
							<span className="itemFontColor">PHP {numComma(price)}</span>
						</Col>
						<Col xs="3" className="p-0">
							<Row className="justify-content-center" noGutters>
								<Col className="p-0 text-center">
									
								</Col>
							</Row>
						</Col>
					</Row>
				</Col>
			</div>
		</Nav.Link>
		</React.Fragment>
	)
};
