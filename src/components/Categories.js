import React from 'react';

import {Row, Col} from 'react-bootstrap';

import { useGlobalData } from '../UserProvider';


import {  Link} from 'react-router-dom';

export default function Categories () {

	const {refreshData, setRefreshData } = useGlobalData();
	
	const goTo = () => { setRefreshData(()=>!refreshData) }
	return (
		<React.Fragment>
			<div className="d-flex catDiv1">
				<div className="d-flex catDiv2 px-1 ">

					<Link 
						to='/item/cars_and_properties'
						onClick={goTo} 
						className="p-0 catDiv3">
						<Col xs={12}>
							<Row className="justify-content-center mb-2">
						 		<img className="img" src="/categories/cars_and_properties.png" alt="..."/>
						 	</Row>
						 	<Row className="justify-content-center text-center">
						 		Cars & Properties
						 	</Row>
						 </Col>
					</Link>
					
					<Link 
						to='/item/mobile_and_electronics'
						onClick={goTo}
						className="p-0 catDiv3">
						<Col xs={12}>
							<Row className="justify-content-center mb-2">
						 		<img className="img" src="/categories/mobile_and_electronics.png"/>
						 	</Row>
						 	<Row className="justify-content-center text-center">
						 		Mobile & Electronics
						 	</Row>
						 </Col>	
					</Link>

					<Link 
						to="/item/jobs_and_services"
						onClick={goTo}
						className="p-0 catDiv3">
						<Col xs={12}>
							<Row className="justify-content-center mb-2">
						 		<img className="img" src="/categories/jobs_and_services.png"/>
						 	</Row>
						 	<Row className="justify-content-center text-center">
						 		Jobs & Services
						 	</Row>
						 </Col>	
					</Link>

					<Link 
						to="/item/home_and_living"
						onClick={goTo} 
						className="p-0 catDiv3">
						<Col xs={12}>
							<Row className="justify-content-center mb-2">
						 		<img className="img" src="/categories/home_and_living.png"/>
						 	</Row>
						 	<Row className="justify-content-center text-center">
						 		Home & Living
						 	</Row>
						 </Col>	
					</Link>

					<Link 
						to="/item/fashion"
						onClick={goTo}
						className="p-0 catDiv3">
						<Col xs={12}>
							<Row className="justify-content-center mb-2">
						 		<img className="img" src="/categories/fashion.png"/>
						 	</Row>
						 	<Row className="justify-content-center text-center">
						 		Fashion
						 	</Row>
						 </Col>	
					</Link>

					<Link 
						to="/item/hobbies_and_games"
						onClick={goTo}
						className="p-0 catDiv3">
						<Col xs={12}>
							<Row className="justify-content-center mb-2">
						 		<img className="img" src="/categories/hobbies_and_games.png"/>
						 	</Row>
						 	<Row className="justify-content-center text-center">
						 		Hobbies & Games
						 	</Row>
						 </Col>	
					</Link>

				</div>
			</div>
		</React.Fragment>
	)
};
