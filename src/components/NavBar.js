import React, { useState, useEffect, useRef} from 'react'; 

import {  Nav, Form, Row, Col, Button, Modal, Spinner, InputGroup } from 'react-bootstrap';

import towns from '../data/towns';
import regions from '../data/regions';
// import countries from '../data/countries';

import { GoSearch } from "react-icons/go";
import { GoogleLogin } from 'react-google-login';
import { RiArrowDownSLine } from "react-icons/ri";
import Swal from 'sweetalert2';

import { Link, useHistory} from "react-router-dom";
// import { useRouteMatch } from "react-router-dom";

// import useLocalStorage from './hooks/useLocalStorage'

import { useConversations } from '../contexts/ConversationsProvider'
import { useGlobalData } from '../UserProvider';


export default function NavBar() {	
	

	const {getAllConversations, setMy} = useConversations()


	// const match = useRouteMatch({
	// 	path: "/",
	// 	strict: true,
	// 	sensitive: true
	// });
	
	
	const { refreshData, setRefreshData, refreshData2, setRefreshData2, show1, setShow1, addressName} = useGlobalData();

	// const [search, setSearch] = useState('')
	// const [userName, setUserName] = useState('')
	
	// const [error1, setError1] = useState(false)
	// const [errMsg1, setErrMsg1] = useState('')

	const [show2, setShow2] = useState(false)
	const [error2, setError2] = useState(false)
	const [errMsg2, setErrMsg2] = useState('')

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [country, setCountry] = useState('')
	const [region, setRegion] = useState('')
	const [town, setTown] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [password3, setPassword3] = useState('')

	const [passVerify, setPassVerify] = useState('')
	const [passFeedback, setPassFeedback] = useState('d-none')
	const [disableButton, setDisableButton] = useState(false)
	
	const [theToken, setTheToken] = useState('')

	const [toggle, setToggle] = useState(false)

	const searchRef = useRef()

	const firstNameRef = useRef()
	const lastNameRef = useRef()
	const emailRef = useRef()
	const mobileNoRef = useRef()
	const countryRef = useRef()
	const regionRef = useRef()
	const townRef = useRef()
	const password1Ref = useRef()
	const password2Ref = useRef()
	const password3Ref = useRef()

	
	let history = useHistory();

	const handleClose1 = () => {
		setShow1(false)
		setEmail('')
		setPassword1('')
		setDisableButton(false)
	};

    const handleShow1 = () => setShow1(true) ;

    const handleClose2 = () => {
    	setShow2(false)
    	setFirstName('')
		setLastName('')
		setEmail('')
		setMobileNo('')
		setCountry('')
		setRegion('')
		setTown('')
		setPassword2('')
		setPassword3('')
		setDisableButton(false)
		setPassVerify('')
		setPassFeedback('d-none')
    };

    const handleShow2 = () => {
    	setShow2(true); 
		setShow1(false);
	};

	
	

	function loginUser (e) {
		e.preventDefault()

		let addr ={
			town:'',
			region:'',
			country:''
		}
		let myAddr = null;
		
		setDisableButton(true)

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: emailRef.current.value+'@starshop.com',
                password: password1Ref.current.value
            })
        })
        .then(res => res.json())
        .then(data => {
        	
            if(typeof data.accessToken !== 'undefined') {

                localStorage.setItem('token', data.accessToken)

                fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
            
                	if(data._id !== null && data._id !== undefined ) {
                		addr ={
							town:data.town,
							region:data.region,
							country:data.country
						}
						myAddr = addressName(addr);

						console.log('myAddr: ', myAddr)
                	

                		// console.log('data:true,', data)
                		localStorage.setItem('userId', data._id)
	                    localStorage.setItem('isActive', data.isActive)
	                    localStorage.setItem('isAdmin', data.isAdmin)
	                    localStorage.setItem('photo', data.photo)
	                    localStorage.setItem('firstName', data.firstName)
	                    localStorage.setItem('lastName', data.lastName)
	                    localStorage.setItem('email', data.email)
	                    localStorage.setItem('country', data.country)
	                    localStorage.setItem('region', data.region)
	                    localStorage.setItem('town', data.town)
	                    localStorage.setItem('mobileNo', data.mobileNo)
	                    localStorage.setItem('createdOn', data.createdOn)
	                    localStorage.setItem('loginType', data.loginType)
	                    

	                    getAllConversations(data.conversations)
	                    setMy({
	                    	id: data._id,
	                    	photo: data.photo,
							firstName: data.firstName,
							lastName: data.lastName,
							email: data.email,
							country: data.country,
							region: data.region,
							town: data.town,
							mobileNo: data.mobileNo,
							createdOn: data.createdOn,
							loginType: data.loginType
	                    })
	                    
	                    setEmail('')
						setPassword1('')
	                    setDisableButton(false)
	                    setShow1(false)

	                    
	                    Swal.fire({
		                  position: 'center',
		                  icon: 'success',
		                  title: 'Logged in',
		                  showConfirmButton: false,
		                  timer: 3000
		                })
		                history.push('/')
	                    
                	} else {
                	
                		 Swal.fire({
		                  position: 'center',
		                  icon: 'warning',
		                  title: 'Encountered an error. Please try again.',
		                  showConfirmButton: false,
		                  timer: 3000
		                })

                		setEmail('')
						setPassword1('')
	                    setDisableButton(false)
	                    setShow1(false)
                		
                		history.push('/')
                		localStorage.clear();
                	}

                })
            } else {
            
                if (data.error==='does-not-exist') {
                    
                    Swal.fire({
	                  position: 'center',
	                  icon: 'warning',
	                  title: 'The email and password you entered did not match our records. Please double-check and try again.',
	                  showConfirmButton: false,
	                  timer: 7000
	                })
	                setEmail('')
					setPassword1('')
                    setDisableButton(false)
                    setShow1(false)
                    history.push('/')

                    
                } else if ( data.error === 'incorrect-password' ) {
                    
                    Swal.fire({
	                  position: 'center',
	                  icon: 'warning',
	                  title: 'Incorrect password, please check and try again.',
	                  showConfirmButton: false,
	                  timer: 7000
	                })
	                setEmail('')
					setPassword1('')
                    setDisableButton(false)
                    setShow1(false)
                    history.push('/')
                    
                } else if ( data.error === 'login-type-error' ) {
                    
                    Swal.fire({
	                  position: 'center',
	                  icon: 'warning',
	                  title: 'YOu may have registered through a different login',
	                  showConfirmButton: false,
	                  timer: 7000
	                })
	                setEmail('')
					setPassword1('')
                    setDisableButton(false)
                    setShow1(false)
                    history.push('/')
                }
                
            }
        })
		
	}
	
	function passwordMatched() {
		const password2 = password2Ref.current.value;
		const password3 = password3Ref.current.value;

		if((password2 !== '' && password3 !== '') && (password2 === password3)){
            setPassVerify('is-valid')
			setPassFeedback('d-none')

			return true
        }else if((password2 !== '' && password3 !== '') && (password2 !== password3)) {
            setPassVerify('is-invalid')
			setPassFeedback('invalid-feedback')
			return false
        } else {
        	setPassVerify('')
			setPassFeedback('d-none')
			return false
        }
	}
	
	function singUpUser (e) {
		e.preventDefault()
		

		setDisableButton(true)
		if(passwordMatched()) {
			
			fetch(`${process.env.REACT_APP_API_URL}/users/email-exists`, {
	            method: 'POST',
	            headers: {
	                'Content-Type' : 'application/json'
	            },
	            body: JSON.stringify({
	                email: email
	            })
			})
			.then(res => res.json())
			.then(data => {
	            if(data=== false) {

	                fetch(`${process.env.REACT_APP_API_URL}/users`, {
	                    method: 'POST',
	                    headers: {
	                        'Content-Type': 'application/json'
	                    },
	                    body: JSON.stringify({
	                        firstName: firstNameRef.current.value,
	                        lastName: lastNameRef.current.value,
	                        email: emailRef.current.value+'@starshop.com',
	                        mobileNo: mobileNoRef.current.value,
	                        country: country,
	                        region: region,
	                        town:town,
	                        password: password2Ref.current.value
	                    })
	                })
	                .then(res => res.json())
	                .then(data => {
	                    if(data=== true) {

							fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
					            method: 'POST',
					            headers: {
					                'Content-Type' : 'application/json'
					            },
					            body: JSON.stringify({
					                email: emailRef.current.value+'@starshop.com',
					                password: password2Ref.current.value
					            })
					        })
					        .then(res => res.json())
					        .then(data => {
					        	
					            if(typeof data.accessToken !== 'undefined') {

					                localStorage.setItem('token', data.accessToken)

					                fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
					                    headers: {
					                        Authorization: `Bearer ${data.accessToken}`
					                    }
					                })
					                .then(res => res.json())
					                .then(data => {

					                    localStorage.setItem('userId', data._id)
					                    localStorage.setItem('isActive', data.isActive)
					                    localStorage.setItem('isAdmin', data.isAdmin)
					                    localStorage.setItem('photo', data.photo)
					                    localStorage.setItem('firstName', data.firstName)
					                    localStorage.setItem('lastName', data.lastName)
					                    localStorage.setItem('email', data.email)
					                    localStorage.setItem('country', data.country)
					                    localStorage.setItem('region', data.region)
					                    localStorage.setItem('town', data.town)
					                    localStorage.setItem('mobileNo', data.mobileNo)
					                    localStorage.setItem('createdOn', data.createdOn)
					                    localStorage.setItem('loginType', data.loginType)

					                    getAllConversations(data.conversations)
					                    setMy({
					                    	id: data._id,
					                    	photo: data.photo,
											firstName: data.firstName,
											lastName: data.lastName,
											email: data.email,
											country: data.country,
											region: data.region,
											town: data.town,
											mobileNo: data.mobileNo,
											createdOn: data.createdOn,
											loginType: data.loginType
					                    })

				                        Swal.fire({
				                          position: 'center',
				                          icon: 'success',
				                          title: 'Registered successfuly',
				                          showConfirmButton: false,
				                          timer: 3000
				                        })

										setPassVerify('')
										setPassFeedback('d-none')
										setDisableButton(false)
										setShow2(false)
										
										history.push("/");
					                })
					            } else {
					            
					                if (data.error==='does-not-exist') {
					                    
					                    Swal.fire({
						                  position: 'center',
						                  icon: 'warning',
						                  title: 'Internal Server Error. (Err:#1)',
						                  showConfirmButton: false,
						                  timer: 7000
						                })
										
										setPassVerify('')
										setPassFeedback('d-none')
										setDisableButton(false)
										setShow2(false)
					                    history.push('/')

					                    
					                } else if ( data.error === 'incorrect-password' ) {
					                    
					                    Swal.fire({
						                  position: 'center',
						                  icon: 'warning',
						                  title: 'Internal Server Error. (Err:#2)',
						                  showConfirmButton: false,
						                  timer: 7000
						                })
										

										setDisableButton(false)
										setShow2(false)
					                    history.push('/')
					                    
					                } else if ( data.error === 'login-type-error' ) {
					                    
					                    Swal.fire({
						                  position: 'center',
						                  icon: 'warning',
						                  title: 'Internal Server Error. (Err:#3)',
						                  showConfirmButton: false,
						                  timer: 7000
						                })
										
										setPassVerify('')
										setPassFeedback('d-none')
										setDisableButton(false)
										setShow2(false)
					                    history.push('/')
					                }
					                
					            }
					        })

	                    } else {

	                    	console.log('Internal server error. Try again.')
							
							setPassVerify('')
							setPassFeedback('d-none')
							setDisableButton(false)
							setShow2(false)
	                    }
	                })

	            } else {
	            	Swal.fire({
	                  position: 'center',
	                  icon: 'warning',
	                  title: 'User already exist',
	                  showConfirmButton: false,
	                  timer: 3000
	                })
	            	console.log('User already exist')
					
					setPassVerify('')
					setPassFeedback('d-none')
					setDisableButton(false)
					setShow2(false)

	            }
			})

			
		} else {
			console.log('Error in password')
			setDisableButton(false)
		}
	}


	const authenticateGoogleToken = (response) => {


        fetch(`${process.env.REACT_APP_API_URL}/users/verify-google-id-token`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({ 
            	tokenId: response.tokenId 
            })
        })
        .then(res => res.json())
        .then(data => {
        	console.log('authenticateGoogleToken:', data)
            
            if(typeof data.accessToken !== 'undefined') {
                
                localStorage.setItem('token', data.accessToken)
                
                retrieveUserDetails(data.accessToken)

            } else {
                if (data.error === 'google-auth-error') {
                  
                    console.log('google-auth-error')
                    
                } else if (data.error === 'google-login-error') {
                  
                    console.log('google-login-error')
                }
            }
        }) 
    }

    const retrieveUserDetails = (accessToken) => {

    	
        const options = {
            headers: { 
                Authorization: `Bearer ${accessToken}`
            }
        }

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, options )
        .then(res => res.json())
        .then(data => {
        	
        	if(data._id !== null && data._id !== undefined ) {
        		// console.log('data:true,', data)
        		localStorage.setItem('userId', data._id)
                localStorage.setItem('isActive', data.isActive)
                localStorage.setItem('isAdmin', data.isAdmin)
                localStorage.setItem('photo', data.photo)
                localStorage.setItem('firstName', data.firstName)
                localStorage.setItem('lastName', data.lastName)
                localStorage.setItem('email', data.email)
                localStorage.setItem('country', data.country)
                localStorage.setItem('region', data.region)
                localStorage.setItem('town', data.town)
                localStorage.setItem('mobileNo', data.mobileNo)
                localStorage.setItem('createdOn', data.createdOn)
                localStorage.setItem('loginType', data.loginType)


                getAllConversations(data.conversations)
                setMy({
                	id: data._id,
                	photo: data.photo,
					firstName: data.firstName,
					lastName: data.lastName,
					email: data.email,
					country: data.country,
					region: data.region,
					town: data.town,
					mobileNo: data.mobileNo,
					createdOn: data.createdOn,
					loginType: data.loginType
                })

                setShow1(false)
                setShow2(false)

                setEmail('')
				setPassword1('')
                setDisableButton(false)

                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Logged in',
                  showConfirmButton: false,
                  timer: 3000
                })

                history.push('/')
                
        	} else {

        		setShow1(false)
        		setShow2(false)

        		setEmail('')
				setPassword1('')
                setDisableButton(false)

        		 Swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'Encountered an error. Please try again.',
                  showConfirmButton: false,
                  timer: 3000
                })
        		 history.push('/')
        		localStorage.clear();

        	}
        })
    }
    
    const sellItem = () => {
    	const email = localStorage.getItem('email')

    	if(String(email) !== 'null' && String(email) !== 'undefined') {
    		history.push('/sell-item') 
    	} else {
    		setShow1(true)
    	}
    }


	useEffect(() => {
        if((password2 !== '' && password3 !== '') && (password2 === password3)){
            setPassVerify('is-valid')
			setPassFeedback('d-none')
        }else if((password2 !== '' && password3 !== '') && (password2 !== password3)) {
            setPassVerify('is-invalid')
			setPassFeedback('invalid-feedback')
        } else {
        	setPassVerify('')
			setPassFeedback('d-none')
        }
        
    }, [password2, password3])

    useEffect(() => {
		localStorage.getItem('userId')
		localStorage.getItem('isActive')
		localStorage.getItem('isAdmin')
		localStorage.getItem('photo')
		localStorage.getItem('firstName')
		localStorage.getItem('lastName')
		localStorage.getItem('email')
		localStorage.getItem('country')
		localStorage.getItem('region')
		localStorage.getItem('town')
		localStorage.getItem('mobileNo')
		localStorage.getItem('createdOn')
		localStorage.getItem('loginType')
		
    }, [refreshData])


    function searchForm(e) {
		e.preventDefault()

		localStorage.removeItem('keyword')

		localStorage.setItem('keyword', searchRef.current.value)

		const currentUrl = window.location.pathname;

		let url = currentUrl.split('/');
		
		url.shift();
		
		if(url[0] !== 'search') {
			
			history.push(`/search/keyword=${localStorage.getItem('keyword')}`)
			
		} else {
			setRefreshData2(()=>!refreshData2)
		}
		
	}


	const selectRegion = regions.map((element, idx)=>{

		if(element.country===country){
			return( <option key={idx} value={element.region}>{element.regionN}</option> )
		}
	})

	const selectTown = towns.map((element, idx)=>{

		if(element.region===region&&element.country===country){
			return( <option key={idx} value={element.town}>{element.townN}</option> )
		}
	})

	const goTo = () => {
		
		console.log('refreshData navbar')
		setRefreshData(()=>!refreshData)
		console.log(refreshData)
		history.push(`/user/${localStorage.getItem('userId')}`)
	}

	const gotoChat = () =>{
		window.location.assign("/chat");
	}

	return (
		<React.Fragment>
		
		<Row className="sticky-top" noGutters>
		<Col xs={12} className="m-0 p-0">
			<div className="topnav">

				<div className="containerDIV">
					
					<div className="contentDIV">
					{ (	localStorage.getItem('email') !== null && 
						localStorage.getItem('email') !== 'undefined') ?
						<div className="__dropdown">
						    <button className="__dropbtn">
							    {`${localStorage.getItem('firstName')}  ${localStorage.getItem('lastName')}`}
							    <RiArrowDownSLine className="__dropicon ml-4"/>
						    </button>

						    <div className="__dropdown-content">
								
								<a type="button" onClick={goTo}>
									Profile
								</a>
								<a type="button" onClick={gotoChat}>
									Inbox
								</a>
								
						    	<Link to="/profile/Settings">
									Settings
								</Link>
						    	<Link to="/logout">
									Log Out
								</Link>
						    </div>
						</div> 
						:
						<React.Fragment>
						    <button className="btn1" onClick={handleShow1}>
						    	Sign In
						    </button>
						    <button className="btn1" onClick={handleShow2}>
						    	Sign Up
						    </button>
					    </React.Fragment>
					}
					</div>
				</div>

				<div className="containerDIV custom1 xsDisplayNone">
					<div className="contentDIV custom2">
						
							
						<Nav.Link href="/" className="p-0 my-2">
							<img href="/" src="/starshoplogo.png" className="logo"/>
						</Nav.Link>
						

						<Form onSubmit={(e) => searchForm(e)} className="form1 ">
				        	
				        	<input 
					        	type="text" 
					        	placeholder="Search.." 
					        	name="search"  
					        	ref={searchRef}
					        	className="search"
					        	required/>
				        	
					    	<button className="buttonStyle" type="submit">
					    		<GoSearch className="iconStyle"/>
					    	</button>
				        </Form>
				       
						<button 
							onClick={sellItem}
							className="buttonStyle sellButton">
							Sell
						</button>
					</div>
				</div>
			</div>
		</Col>
		</Row>


		<Modal
            show={show1}
            onHide={handleClose1}
            animation={false}
            centered
            className="customized-modal-1"
          >
            <Modal.Header closeButton>
              <Modal.Title>Log In</Modal.Title>
            </Modal.Header>
            <Modal.Body  className="modalBox">
                
            <Form onSubmit={(e) => loginUser(e)}>

                <Form.Group controlId="userName" className="nav_aa">
                	<div className="px-0 nav_ab">
	                    <Form.Control type="text" 
		                    placeholder="Email" 
		                    ref={emailRef} required/>
		            </div>
		            <div className="px-0 nav_ac">
		                <InputGroup.Prepend>
				        	<InputGroup.Text  className="nav_ad">@starshop.com</InputGroup.Text>
				        </InputGroup.Prepend>
                	</div>
                </Form.Group>

                {<Form.Group controlId="password1">
                    <Form.Control 
                    	type="password" 
                    	placeholder="Password" 
                    	ref={password1Ref} required/>
                </Form.Group>}
                <Row className="justify-content-center mt-5" noGutters>
                    <Col>
		                {disableButton ? 
		                	<Button variant="warning" type="submit" id="submitBtn1" block disabled>
		                	Log In
		                	<Spinner 
		                		animation="border" 
		                		variant="secondary"
		                		className="ml-4" 
		                		size="sm" 
		                		role="status"/>
		                </Button>
		                :
		                <Button variant="warning" type="submit" id="submitBtn1" block>
		                	Log In
		                </Button>}      
                    </Col>
                </Row>
                <Row className="justify-content-center my-1" noGutters>
                <span>or</span>
                </Row>
                <Row className="justify-content-center my-1" noGutters>
                    <Col>
                        <GoogleLogin
                       clientId="380992695096-k7shc67trlq8dbkorchrge335qmb8krn.apps.googleusercontent.com"
                            buttonText="Google Log In"
                            onSuccess= { authenticateGoogleToken }
                            onFailure= { authenticateGoogleToken }
                            cookiePolicy= { 'single_host_origin' }
                            className= { "w-100 text-center d-flex justify-content-center" }

                        />
                    </Col>
                </Row>
            </Form>
            <Row className="justify-content-center mt-5" noGutters>
	            <span>New member? </span> 
	                <button className="btn3" id="submitBtn2" onClick={ handleShow2}>
			        	Sign Up
			        </button>
		        <span>here</span>
	        </Row>
            </Modal.Body>
        </Modal>

        <Modal
            show={show2}
            onHide={handleClose2}
            centered 
            className="customized-modal-1"
          >
            <Modal.Header closeButton>
              <Modal.Title>Sign Up</Modal.Title>
            </Modal.Header>
            <Modal.Body  className="modalBox">
           
            <Form onSubmit={(e) => singUpUser(e)}>
            	<Form.Group controlId="firstName">
                    <Form.Control type="text" 
                    placeholder="First name" 
                    ref={firstNameRef} required/>
                </Form.Group>

                <Form.Group controlId="lastName">
                    <Form.Control type="text" 
                    placeholder="Last name" 
                    ref={lastNameRef} required/>
                </Form.Group>

                <Form.Group controlId="email" className="nav_aa">
                	<div className="px-0 nav_ab">
	                    <Form.Control type="text"
		                    placeholder="Email" 
		                    ref={emailRef} required/>
		            </div>
		            <div className="px-0 nav_ac">
		                <InputGroup.Prepend>
				        	<InputGroup.Text  className="nav_ad">@starshop.com</InputGroup.Text>
				        </InputGroup.Prepend>
                	</div>
                </Form.Group>

                <Form.Group controlId="mobileNo">
                    <Form.Control type="number" 
                    placeholder="Enter 11-digit number" 
                    ref={mobileNoRef} required/>
                </Form.Group>

                <Form.Group controlId="country">
	                <Form.Control 
	                	as="select" 
	                	onChange={e => setCountry(e.target.value)}
	                	required> 
				        <option value="">Select country</option>
				        <option value="philippines">Philippines</option>
				    </Form.Control>
				</Form.Group>

				<Form.Group controlId="region">
				    <Form.Control 
				    	as="select" 
				    	onChange={e => setRegion(e.target.value)}
				    	required>
				        <option value="">Select region</option>
				        {selectRegion}
				    </Form.Control>
				</Form.Group>

				<Form.Group controlId="town">
				    <Form.Control 
				    	as="select" 
				    	onChange={e => setTown(e.target.value)}
				    	required>
				        <option value="">Select town</option>
				     	{selectTown}
				    </Form.Control>
				</Form.Group>


                <Form.Group controlId="password2">
                    <Form.Control 
                    	type="password" 
                    	placeholder="Password" 
                    	ref={password2Ref}
                    	required/>
                </Form.Group>

                <Form.Group controlId="password3">
                    <Form.Control 
                    	className={`${passVerify}`}
                    	type="password" 
                    	placeholder="Verify password" 
                    	ref={password3Ref}
                    	required/>

                    <div className={`${passFeedback}`}>
			        	Password not matched
			        </div>
                </Form.Group>

                <Row className="justify-content-center mt-5" noGutters>
                    <Col>
		                {disableButton ? 
		                	<Button variant="warning" type="submit" id="submitBtn3" block disabled>
		                	Sign Up 
		                	<Spinner 
		                		animation="border" 
		                		variant="secondary"
		                		className="ml-4" 
		                		size="sm" 
		                		role="status"/>
		                </Button>
		                :
		                <React.Fragment>
		                
			                <Button variant="warning" type="submit" id="submitBtn3" block>
			                	Sign Up 
			                </Button>
		            	
		                </React.Fragment>
		            }
                    </Col>
                </Row>
                
                <Row className="justify-content-center my-1" noGutters>
                <span>or</span>
                </Row>
                <Row className="justify-content-center my-1" noGutters>
                    <Col>
                        <GoogleLogin
                            
                  clientId="380992695096-k7shc67trlq8dbkorchrge335qmb8krn.apps.googleusercontent.com"
                            buttonText="Google"
                            onSuccess= { authenticateGoogleToken }
                            onFailure= { authenticateGoogleToken }
                            cookiePolicy= { 'single_host_origin' }
                            className= { "w-100 text-center d-flex justify-content-center" }

                        />
                    </Col>
                </Row>
            </Form>
            </Modal.Body>
            <Modal.Footer>
			</Modal.Footer>
        </Modal>

		</React.Fragment>
	)
}				        		